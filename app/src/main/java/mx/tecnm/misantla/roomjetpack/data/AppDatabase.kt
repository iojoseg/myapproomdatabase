package mx.tecnm.misantla.roomjetpack.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

// RoomDatabase
// nuestro manejador de base datos se va encargar de crear tu BD por 1 vez

@Database(
    entities = [Mascota::class],version = 1, exportSchema = false)
    //definir todas tus entidades separadas por comas
   // entities = [Mascota::class,Persona::class]

abstract class AppDatabase : RoomDatabase(){
    //Registar todos los DAO de mi aplicacion para tener un referencia
    abstract fun mascotaDao(): MascotaDao

    companion object{                     // instancia el manejado de la BD
                                          // ? el simbolo una variable segura
        private const val DATABASE = "bd_mascotas"
        private var instancia: AppDatabase? = null

        fun getInstancia(context:Context):AppDatabase{
            if(instancia == null) // la base Datos es nula se debe crear la BD
            {
                instancia = Room.databaseBuilder(context,
                    AppDatabase::class.java,
                    DATABASE).build()
            }
            return instancia as AppDatabase    //si existe solo la retorno
        }
    }
}