package mx.tecnm.misantla.roomjetpack.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

//----------
/*data class Mascota (
    val codigo:Int,
    val nombres:String,
    val raza:String,
    val preferencia:String)
*/
//-----------


// Room dice que este data class se va a convertir en una tabla
@Entity(tableName = "tablaMascota")
data class Mascota (

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="codigo")
    val codigo:Int,
    @ColumnInfo(name="nombre")
    val nombre:String,
    @ColumnInfo(name="raza")
    val raza:String,
    @ColumnInfo(name="preferencia")
    val preferencia:String)

/* Solo se va hacer modelo de datos
* @Ignore
* val nombreRaza:String
* */