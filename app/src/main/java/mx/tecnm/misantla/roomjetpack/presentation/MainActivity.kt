package mx.tecnm.misantla.roomjetpack.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import mx.tecnm.misantla.roomjetpack.R
import mx.tecnm.misantla.roomjetpack.data.AppDatabase
import mx.tecnm.misantla.roomjetpack.databinding.ActivityMainBinding
import mx.tecnm.misantla.roomjetpack.databinding.ActivityRegistroMascotasBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private val appDatabase by lazy {
        AppDatabase.getInstancia(this)
    }

    private val mascotaAdapter by lazy {
        MascotaAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        setUpAdapter()

    }

    private fun init(){
        appDatabase?.mascotaDao()?.getAlls().observe(this, Observer {
            mascotaAdapter.updateData(it)
        })

        binding.fabAgregar.setOnClickListener {
            irRegistro()
        }
    }
    private fun irRegistro(){
        val intent = Intent(this,RegistroMascotas::class.java)
        startActivity(intent)
    }
    private fun setUpAdapter(){
        binding.recyclerMascotas.adapter = mascotaAdapter
        binding.recyclerMascotas.layoutManager = LinearLayoutManager(this)
    }
}