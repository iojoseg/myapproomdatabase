package mx.tecnm.misantla.roomjetpack.data

import androidx.lifecycle.LiveData
import androidx.room.*

// Los Dao son interfaces
// cada Entidad tiene su propio DAO
@Dao
interface MascotaDao {

     @Insert
     suspend fun insert(mascota:Mascota):Long

/*
    @Insert
    fun insertar(mascota:Mascota)
*/

    @Update
    fun actualizar(mascota:Mascota)

    @Delete
    fun eliminar(mascota:Mascota)

    @Query("select *from tablaMascota")
    fun getAlls(): LiveData<List<Mascota>>


  //  @Query("select *from tablaMascota")
  //  fun obtenerMascotas() :List<Mascota>

}