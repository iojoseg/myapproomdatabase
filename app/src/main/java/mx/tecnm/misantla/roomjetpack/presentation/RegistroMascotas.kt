package mx.tecnm.misantla.roomjetpack.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mx.tecnm.misantla.roomjetpack.data.AppDatabase
import mx.tecnm.misantla.roomjetpack.data.Mascota
import mx.tecnm.misantla.roomjetpack.databinding.ActivityRegistroMascotasBinding

class RegistroMascotas : AppCompatActivity() {

    private lateinit var binding: ActivityRegistroMascotasBinding
    private val appDatabase by lazy{
        AppDatabase.getInstancia(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegistroMascotasBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init(){
        binding.btnRegistrar.setOnClickListener {
            val nombre = binding.edtNombre.text.toString()
            val raza = binding.edtRaza.text.toString()
            val preferencia = binding.edtPreferencia.text.toString()

            val mascota = Mascota(0,nombre,raza,preferencia)


            lifecycleScope.launch{
                val respuesta = withContext(Dispatchers.IO){
                    appDatabase?.mascotaDao()?.insert(mascota)
                }
                if (respuesta.toInt() > 0) mensaje("Mascota creada") else mensaje("Hubo un problema registrar su mascota")
                onBackPressed()
            }
        }
    }

    fun mensaje(mensaje:String){
        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show()
    }
}